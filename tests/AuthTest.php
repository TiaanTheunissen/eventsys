<?php

use App\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{

    use DatabaseTransactions;

    /** @test */
    function a_user_can_register_for_account_but_must_confirm_their_email_address(){
        $this->visit('register')
                ->type('Tiaan', 'name')
                ->type('Tiaan@saiba.org.za', 'email')
                ->type('password', 'password')
                ->type('password', 'password_confirmation')
                ->press('register');

        $this->see('Please confirm your email address')
                ->seeInDatabase('users', ['name' => 'Tiaan', 'verified' => 0]);

        $user = User::where('name', 'Tiaan')->first();

        $this->visit("register/confirm/{$user->token}")
            ->see('You are now confirmed')
            ->seeinDatabase('users', ['name' => 'Tiaan', 'verified' => '1']);
    }

    protected function login($user = null){
        $user = $user ?: $this->factory->create('App\User', ['password' => 'password']);

        return $this->visit('login')
                ->type($user->email, 'email')
                ->type($user->password, 'password')
                ->press('login');

        $this->see('my account');
    }
}
