@include('frontend.partials.header')
@include('frontend.partials.nav')

@if(Request::is('/', 'profiles/{profile}'))

@else
    <div class="breadcrumbs">
        <div class="container">
            <h1 class="pull-left">@yield('title')</h1>
            <span class="pull-right hidden-xs hidden-md">
                @yield('breadcrumbs')
            </span>
        </div>
    </div>
@endif

@yield('content')

@if($errors->any())
    @foreach($errors->all() as $error)
        <div class="row">
            <div class="col-md-12 clearfix">
                {{ Toastr::warning($error, $title = null, $options = []) }}
            </div>
        </div>
    @endforeach
@endif

@if (Session::has('flash_notification.message'))
    <?php
    $level = Session::get('flash_notification.level');
    if($level == 'danger')
        $level = 'error';
    Toastr::$level(Session::get('flash_notification.message'))
    ?>
@endif



@include('frontend.partials.top-footer')
@include('frontend.partials.footer')