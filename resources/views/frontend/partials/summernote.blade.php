<script>
    $(document).ready(function() {
        $('.description').summernote({
            placeholder: 'Write your Biography',
            height: 200,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true,                  // set focus to editable area after initializing summernote
            toolbar: [

                ['style', ['bold','underline']],
                ['font', ['strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph', 'style']],
            ]
        });
    });
</script>