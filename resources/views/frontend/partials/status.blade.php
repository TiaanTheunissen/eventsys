<div class="col-md-12">
    {!! Form::open(['method' => 'post', 'route' => ['profile.status_update', CurrentUser()->slug]]) !!}
    <div class="form-group">
        {!! Form::textarea('status', null, ['class' => 'form-control textarea-expandable', 'rows' => '1', 'placeholder' => 'What\'s on your mind?' ]) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Update Status', ['class' => 'btn btn-u pull-right']) !!}
    </div>
    {!! Form::close() !!}
    <div class="margin-bottom-20"></div>
</div>