@if(auth()->check())
    @if($user->id === CurrentUser()->id)
        {!! Form::open(['method' => 'post', 'route' => ['profile.status_destroy', $post->id]]) !!}
        <button type="submit" class="close">×</button>
        {!! Form::close() !!}
    @endif
@endif