<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>Practice Management Conference</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Eventsys">
    <meta name="author" content="Tiaan Theunissen">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/css/base.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/headers/header-v6.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/footers/footer-v2.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/animate.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/sky-forms-pro/skyforms/css/sky-forms.css">
    <link rel="stylesheet"
          href="/assets/frontend/smarty/assets/plugins/sky-forms-pro/skyforms/custom/custom-sky-forms.css">
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/sky-forms-pro/skyforms/css/sky-forms-ie8.css">
    <![endif]-->

    <!-- CSS Theme -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/theme-colors/default.css" id="style_color">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/theme-skins/dark.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/custom.css">

    {{--<!-- CSS Implementing Plugins -->--}}
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/fancybox/source/jquery.fancybox.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet"
          href="/assets/frontend/smarty/assets/plugins/master-slider/masterslider/style/masterslider.css">
    <link rel='stylesheet'
          href="/assets/frontend/smarty/assets/plugins/master-slider/masterslider/skins/black-2/style.css">

    {{--<!-- CSS Pages Style -->--}}
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/pages/page_one.css">

    {{--<!-- CSS Theme -->--}}
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/theme-colors/default.css" id="style_color">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/custom.css">

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/pages/page_log_reg_v4.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/pages/page_pricing.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/pages/page_clients.css">

    <!-- Summernote -->
    <link href="/assets/frontend/js/summernote.css" rel="stylesheet">

    <!-- Toastr -->
    <link href="/css/toastr.css" rel="stylesheet">
</head>

@if(Request::is('/'))
    <body class="header-fixed">
    @else
        <body class="header-fixed header-fixed-space">
        @endif
        <div class="wrapper">

            <div id="popup-container">
                <a class="close" style="opacity: 1">x</a>
                <div id="popup-window">
                    <div class="splash-bg">
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi architecto beatae, deserunt
                            dolore eligendi impedit incidunt, labore magni neque optio perferendis quis repellat sequi.
                            Iusto quod reprehenderit rerum sapiente voluptatem.
                        </p>
                    </div>
                </div>
            </div>
