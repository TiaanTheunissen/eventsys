</div><!--/wrapper-->
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/jquery/jquery-migrate.min.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- JS Implementing Plugins -->
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/back-to-top.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/smoothScroll.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/jquery.parallax.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/master-slider/masterslider/masterslider.min.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/master-slider/masterslider/jquery.easing.min.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/counter/waypoints.min.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/counter/jquery.counterup.min.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/js/plugins/fancy-box.js"></script>

<script type="text/javascript" src="/assets/frontend/smarty/assets/js/plugins/owl-carousel.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/js/plugins/master-slider-fw.js"></script>
<script src="/assets/frontend/js/jquery.confirm.js"></script>
<script src="/assets/admin/dist/js/bootstrap-confirm-delete.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/backstretch/jquery.backstretch.min.js"></script>

<!-- JS Customization -->
<script type="text/javascript" src="/assets/frontend/smarty/assets/js/custom.js"></script>
<!-- JS Page Level -->
<script type="text/javascript" src="/assets/frontend/smarty/assets/js/app.js"></script>
<script src="/assets/frontend/js/summernote.js"></script>
<!-- additonal page scripts -->

<script type="text/javascript" src="/assets/frontend/smarty/assets/js/forms/contact.js"></script>
<script type="text/javascript" src="/assets/frontend/smarty/assets/js/pages/page_contacts.js"></script>

<!-- Toastr -->
<script src="/assets/admin/dist/js/toastr.min.js" type="application/javascript"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        App.init();
        OwlCarousel.initOwlCarousel();
    });

    toastr.options = {
        "closeButton": true,
        "preventDuplicates": true,
        "newestOnTop": true,
        "positionClass": "toast-bottom-right",
        "progressBar": true,
    };
</script>

{!! Toastr::render() !!}

<script src="/js/jquery.cookie.js"></script>

<!--[if lt IE 9]>
<script src="/assets/frontend/smarty/assets/plugins/respond.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/html5shiv.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

@yield('scripts')
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-63646598-2', 'auto');
    ga('send', 'pageview');

</script>
{{--<script src="/js/popup.js"></script>--}}
</body>
</html>