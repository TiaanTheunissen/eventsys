<div class="footer-v2" id="footer-v2">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-3 md-margin-bottom-40">
                    <a href="{{ route('home') }}">
                        <img id="logo-footer" style="width: 30%" class="footer-logo" src="/assets/frontend/images/logo.png" alt="">
                    </a> <br>
                    @if($about)
                        {!! str_limit($about->description, 300) !!}
                    @endif
                </div><!--/col-md-3-->
                <!-- End About -->

                <!-- Latest -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="posts">
                        <div class="headline"><h2>New Members</h2></div>
                        <ul class="list-unstyled link-list latest-list">
                            @foreach($new_members as $member)
                                <li>
                                    <a href="{{ route('profile.profile', $member->slug) }}" class="no-hover">{{ $member->name }}</a>
                                    <small>Join date: {{ \Carbon\Carbon::parse($member->created_at)->diffForHumans() }}</small>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div><!--/col-md-3-->
                <!-- End Latest -->

                <!-- Link List -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><h2>Useful Links</h2></div>
                    <ul class="list-unstyled link-list">
                        <li><a href="{{ route('schedule') }}">Agenda</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="{{ route('about.show') }}">About us</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="{{ route('members.show') }}">Members</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="{{ route('about.speakers') }}">Speakers</a><i class="fa fa-angle-right"></i></li>
                        <li><a href="{{ route('contact') }}">Contact us</a><i class="fa fa-angle-right"></i></li>
                    </ul>
                </div><!--/col-md-3-->
                <!-- End Link List -->

                <!-- Address -->
                <div class="col-md-3 map-img md-margin-bottom-40">
                    <div class="headline"><h2>Contact Us</h2></div>
                    @if($eventInformation)
                        <address class="md-margin-bottom-40">
                            {{ $eventInformation->street_address }}
                            {{ $eventInformation->venue_city }}, {{ $eventInformation->venue_country }} <br>
                            Phone: {{ $eventInformation->contact_number }} <br>
                            {{--Alternative: {{ $eventInformation->alternative_contact_number }} <br>--}}
                            Email: <a href="mailto:{{ $eventInformation->email_address }}" class="">{{ $eventInformation->email_address }}</a>
                        </address>
                    @endif
                </div><!--/col-md-3-->
                <!-- End Address -->
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>
                        {{ \Carbon\Carbon::now()->year }} © All Rights Reserved.
                        <a href="{{ route('home') }}">Eventsys</a>
                    </p>
                </div>

                <!-- Social Links -->
                <div class="col-md-6">
                    <ul class="footer-socials list-inline">
                        @foreach($socials as $social)
                        <li>
                            <a href="{{$social->url}}" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                <i class="fa {!! $social->icon !!}"></i>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <!-- End Social Links -->
            </div>
        </div>
    </div><!--/copyright-->
</div>