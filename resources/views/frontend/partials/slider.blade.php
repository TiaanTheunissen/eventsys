@if(count($slides))
    @foreach($slides as $slider)
        <div class="interactive-slider-v2 img-v1" style="background-image: url({{ $slider->thumbnail }})">
            <div class="container">
                <h1 style="text-transform: capitalize">{{ $slider->title }}</h1>
                <p>{!! $slider->date_location !!}</p>
                <p><small>Brought to you by</small></p>
                <a href="https://quickbooks.intuit.com/za/accountants/" target="_blank"><img src="https://imagizer.imageshack.com/v2/280x200q90/921/x3wc40.png" alt="Logo"></a>
            </div>
        </div>
    @endforeach
@else
    <div class="interactive-slider-v2 img-v1">
        <div class="container">
            <h1>Eventsys</h1>
            <p>Next event date & Venue</p>
        </div>
    </div>
@endif