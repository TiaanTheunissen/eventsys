@extends('layouts.frontend')

@section('title')
    About Us
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('about') !!}
@endsection

@section('content')
    <div class="container content height-500">
        @if($about)
            <div class="row">
                <div class="col-md-7">
                    <div class="headline"><h2>{!! $about->title !!}</h2></div>
                    {!! $about->description !!}
                </div>
                <div class="col-md-5">
                    <img class="img-responsive img-bordered full-width" src="{!! $about->thumbnail !!}" alt="The Conference">
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    @include('frontend.errors.error', ['text' => 'No Content found'])
                </div>
            </div>
        @endif
    </div>
@endsection