@extends('layouts.frontend')

@section('title')
    <span style="text-transform: uppercase">Conference Schedule</span>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('schedule') !!}
@endsection

@section('content')
    <div class="container content height-500">
        @if(count($days))
            <div class="tab-v1">

                <ul class="nav nav-pills nav-justified header_tabs">
                    @foreach($days as $day)
                        <li class="">
                            <a class="header_title" href="#{{$day->id}}"  data-toggle="tab">
                                <strong>{{ $day->title }}</strong> <br>
                                {{\Carbon\Carbon::parse($day->date)->format('d F')}}
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content">
                    @foreach($days as $day)
                        <div class="tab-pane fade in" id="{{ $day->id }}">

                            <ul class="nav nav-pills nav-justified">
                                <li class="active"><a class="text-center" data-toggle="tab" href="#plenary{{$day->id}}"><strong>Plenary Session</strong></a></li>
                                @foreach($day->streams as $stream)
                                    <li><a class="text-center" data-toggle="tab" href="#stream{{ $stream->id }}"><strong>{{$stream->title}}</strong></a></li>
                                @endforeach
                            </ul>

                            <div class="tab-content">
                                <div id="plenary{{$day->id}}" class="tab-pane fade in active">
                                    @if(count($day->panels))
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="panel-group acc-v1" id="day_parent{{$day->id}}">
                                                    @foreach($day->panels as $plenary)
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <a class="panel-title" data-toggle="collapse" data-parent="#day_parent{{$day->id}}"
                                                                   style="text-decoration: none" href="#thisPanel{!!$plenary->id!!}">
                                                                    {{ $plenary->title }}
                                                                    <span class="pull-right">
                                                                       <span aria-hidden="true" class="icon-clock"></span> {{date('G:i', strtotime($plenary->time))}}
                                                                    </span>
                                                                </a>
                                                            </div>

                                                            @if(! $plenary->isEmpty())
                                                                <div id="thisPanel{!!$plenary->id!!}" class="panel-colapse collapse">
                                                                        <div class="panel-body">
                                                                            {!! ($plenary->description)? : "" !!}
                                                                            <div class="speaker">
                                                                                <br>
                                                                                @if(count($plenary->speaker))
                                                                                    @foreach($plenary->speaker as $speaker)
                                                                                        <div class="notification">
                                                                                            <img class="rounded-x mCS_img_loaded" src="{!! $speaker->SpeakerImage() !!}" alt="">
                                                                                            <div class="overflow-h">
                                                                                            <span>
                                                                                                <a href="{{ route('about.speakers.show', $speaker->slug) }}" style="text-decoration: none; color: #555";>{!! $speaker->full_name .', '. $speaker->job_title .', '. $speaker->organisation !!}</a>
                                                                                            </span>
                                                                                            </div>
                                                                                        </div>
                                                                                    @endforeach
                                                                                @else
                                                                                    <p>Sorry, no speakers listed at the moment.</p>
                                                                                @endif
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <div class="row">
                                            <div class="col-md-12">
                                               @include('frontend.errors.error', ['text' => 'No Content Found'])
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                @foreach($day->streams as $stream)
                                    <div id="stream{{$stream->id}}" class="tab-pane fade in">
                                        @if(count($stream->panels))
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="panel-group acc-v1" id="stream_data_parent_{{$stream->id}}">
                                                        @foreach($stream->panels as $panel)
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <a class="accordion-toggle panel-title" data-toggle="collapse" data-parent="#stream_data_parent_{{$stream->id}}"
                                                                       style="text-decoration: none" href="#stream_panel_{{$panel->id}}">
                                                                        {{ $panel->title }}
                                                                        <span class="pull-right">
                                                                            <span aria-hidden="true" class="icon-clock"></span> {{date('G:i', strtotime($panel->time))}}
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                                @if(! $panel->isEmpty())
                                                                    <div id="stream_panel_{{$panel->id}}" class="panel-collapse collapse">
                                                                        <div class="panel-body">
                                                                            {!! $panel->description !!}
                                                                            <div class="speaker">
                                                                                <br>
                                                                                @foreach($panel->speakers as $speaker)
                                                                                    <div class="notification">
                                                                                        <img class="rounded-x mCS_img_loaded" src="{!! $speaker->SpeakerImage() !!}" alt="">
                                                                                        <div class="overflow-h">
                                                                                        <span>
                                                                                            <a href="{{ route('about.speakers.show', $speaker->slug) }}" style="text-decoration: none; color: #555";>{!! $speaker->full_name .', '. $speaker->job_title .', '. $speaker->organisation !!}</a>
                                                                                        </span>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @include('frontend.errors.error', ['text' => 'No Content found'])
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-12">
                    <p>The full agenda for this year’s event will be released very soon. The topics that will be discussed on 14 November are listed below. If you are interested in presenting or being part of a panel discussion on any of these topics, please contact Ros Hinchcliffe,
                        <a href="mailto:ros@accountingacademy.co.za">ros@accountingacademy.co.za</a></p>
                    <p><strong>International keynote address: The Practice Growth Blueprint – how to take control of your practice, make more money and get your life back</strong></p>
                    <ul>
                        <li>Why you’re operating on a broken model and how it’s bleeding you time and money every year </li>
                        <li>The Practice Growth Blueprint – the 9-step system my clients use to make more money, get their time back and achieve freedom</li>
                        <li>The 3 keys to predictable practice growth and how to systemise these to increase your profits year on year</li>
                        <li>The ‘Profitable Practice Model’ that extracts you from the middle of the practice and allows you to work less</li>
                        <li>The step-by-step process for creating scalable systems that enable you to go hands-free and give you freedom </li>
                    </ul>
                    <p><strong>Session One: Planning for your practice</strong></p>
                    <ul>
                        <li>
                            Identifying the types of planning you need to do:
                            <ul>
                                <li>strategic planning</li>
                                <li>planning for different functions of your firm: service delivery, risk management, human resources, marketing, technology, finance and administration</li>
                            </ul>
                        </li>
                        <li>Ensuring that your planning strategy aligns with the resources you have available</li>
                        <li>Working with your partners to lay down a strong memorandum of understanding</li>
                        <li>Allocating clear roles, responsibilities and reporting lines within your team </li>
                        <li>Ensuring that your initial client base and service offerings do not exceed your capacity</li>
                        <li>Balancing new clients and services with the ongoing running of the business to achieve growth at a sustainable pace</li>
                    </ul>
                    <p><strong>Session Two: Practice models and networks</strong></p>
                    <ul>
                        <li>Looking at the structural considerations inherent in owning or running an accounting practice: assessing different profit-sharing and decision-making models </li>
                        <li>Identifying what business model to use: the pros and cons of traditional accounting services vs. high-end advisory services</li>
                        <li>Identifying the benefits and drawbacks of outsourcing certain functions of an accounting practice</li>
                        <li>Value-added services: developing profitable practice or service area niches</li>
                        <li>Using networks to add value and grow profitability</li>
                    </ul>
                    <p><strong>Session Three: Building and growing your practice</strong></p>
                    <ul>
                        <li>Deciding whether your firm should grow: if so, by how much? </li>
                        <li>Setting attainable goals for your firm’s growth</li>
                        <li>
                            Deciding whether to grow alone or to merge with another small practice:
                            <ul>
                                <li>what factors do you need to consider?</li>
                            </ul>
                        </li>
                        <li>
                            Analysing the strengths and weaknesses of your existing client base:
                            <ul>
                                <li>which businesses have the potential to grow with you?  </li>
                                <li>which ones are costing you too much in time and resources?</li>
                            </ul>
                        </li>
                        <li>Developing and implementing a marketing strategy to support your growth goals</li>
                        <li>Managing your growth: what should you do when you hit a ceiling?</li>
                    </ul>
                    <p><strong>Session Four: Developing a people management strategy</strong></p>
                    <ul>
                        <li>
                            Building a win-win people management strategy:
                            <ul>
                                <li>tools for improving performance and motivation	- compensation and benefits</li>
                                <li>training</li>
                                <li>workplace rights</li>
                                <li>staff appraisals</li>
                                <li>delegation</li>
                                <li>promotion and career development </li>
                                <li>recognition</li>
                            </ul>
                        </li>
                        <li>Identifying common people management pitfalls and how to avoid them</li>
                        <li>Tools for improving employer-staff relationships and teamwork and morale across the practice</li>
                        <li>Key factors to consider when recruiting new staff</li>
                    </ul>
                    <p><strong>Session Five: Cloud accounting and other disruptive technologies </strong></p>
                    <p>This panel will look at disruptive technologies which are transforming traditional accounting and compliance-based services, forcing practitioners to expand and diversify their service offerings.   Panellists will discuss the threats, capabilities and opportunities of the new technology landscape, such as:</p>
                    <ul>
                        <li>The trend towards cloud accounting</li>
                        <li>Data analytics</li>
                        <li>The Internet of Things (IoT)</li>
                        <li>Mobile applications </li>
                        <li>Social media </li>
                        <li>Blockchain </li>
                        <li>Artifical Intelligence (AI)</li>
                    </ul>
                    <p><strong>Session Six: Client relationship management</strong></p>
                    <ul>
                        <li>Knowing how to communicate effectively with your client base: what works and what doesn’t?</li>
                        <li>
                            Factors to consider when taking on new clients
                            <ul>
                                <li>will you be able to retain them? </li>
                                <li>Can you effectively meet their needs?</li>
                            </ul>
                        </li>
                        <li>Ensuring that long-standing clients receive as much attention as new ones</li>
                        <li>
                            Seizing opportunities for enhanced client relationships through new services, despite limited resources:
                            <ul>
                                <li>making the most of networks, alliances and referrals</li>
                            </ul>
                        </li>
                        <li>Tools and technologies for improving client relationships and ensuring client retention </li>
                        <li>Understanding clients’ reasons for leaving and what lessons can be learned to prevent future attrition</li>
                    </ul>
                    <p><strong>Session Seven: Risk management</strong></p>
                    <ul>
                        <li>Developing a framework and processes for identifying, evaluating, and acting on risks within your practice</li>
                        <li>Examining ethical issues and safeguards which can be used to deal with ethical threats</li>
                        <li>Understanding the role of quality control systems standard procedures and ensuring that your employees are fully aware of them</li>
                        <li>What risk mitigation strategies or tools need to be put in place, e.g. insurance, data back-up, etc.?</li>
                    </ul>
                    <p><strong>Session Eight: Succession planning</strong></p>
                    <ul>
                        <li>Understanding the importance of succession planning and the risks of not having a solid succession plan in place</li>
                        <li>Analysing the structure of your firm – can it withstand the loss of exiting partners and smoothly assimilate new ones?</li>
                        <li>Identifying key personnel who have the potential to develop within the practice and support its sustainability</li>
                        <li>Ensuring thorough handovers when staff and partners exit the firm to ensure business continuity and minimum interruption to client service </li>
                        <li>Maintaining strong relationships with clients when their accountant leaves the practice</li>
                        <li>Planning what steps to take when closing, selling or merging the practice</li>
                    </ul>
                    {{--@include('frontend.errors.error', ['text' => 'No Schedule found'])--}}
                </div>
            </div>
        @endif

        <div class="pull-right">
            @if($file)
                <a href="{{$file->file}}" target="_blank" download="{{$file->title}}" class="btn btn-default"><i class="fa fa-file-pdf-o"></i> Download schedule</a>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function(){
            $(document).on('click','a.drop-info',function(e){
                ($(this))
            });
        });

//        $(function() {
////            $( "#thisPanel1" ).panel({'collapsible':true,'active':false});
//        });

        $(".nav-pills li:first").addClass("active");
        @if(count($days))
            $('#{{\App\Models\Day::orderBy('date')->first()->id}}').addClass("active")
        @endif
    </script>
@endsection
