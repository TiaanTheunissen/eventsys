@extends('layouts.frontend')

@section('title')
    Speakers
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('speakers') !!}
@endsection

@section('content')

    <div class="container content height-500">
            <div class="row">
                <div class="col-md-12">
                    @unless(count($speakers))
                        @include('frontend.errors.error', ['text' => 'No Speakers found'])
                    @endif
                    @foreach($speakers->chunk(4) as $chunk)
                        <div class="row team-v5 margin-bottom-30">
                            @foreach($chunk as $speaker)
                                <div class="col-sm-3 sm-margin-bottom-50">
                                    <div class="team-img">
                                        <img class="img-responsive img-bordered" src="{{ $speaker->SpeakerImage() }}" alt="">
                                        <div class="team-hover">
                                            <a href="{{ route('about.speakers.show', $speaker->slug) }}" class="bnt btn-u">More Information</a>
                                        </div>
                                    </div>
                                    <span>{{str_limit($speaker->full_name, '20')}}</span>
                                    <small>{{str_limit($speaker->organisation, 30)}}</small>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
    </div>
@endsection