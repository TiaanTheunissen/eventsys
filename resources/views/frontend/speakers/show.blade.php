@extends('layouts.frontend')

@section('content')

@section('title')
    Speakers
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('speakers.show', $speaker->full_name) !!}
@endsection

<div class="container content height-500">
    <div class="row">
        <div class="row">
            <div class="col-md-3">
                <img width="100%" class="img-bordered" src="{{$speaker->SpeakerImage()}}">

                <div class="text-center">
                    <div class="hidden">
                        {{ $percent = (round($speaker->averageRating()->first(), 0) / 100) * 1000 }}
                    </div>
                    <div class="margin-bottom-20"></div>
                    <div class="star-ratings-sprite"><span style="width:{{ $percent }}%" class="rating"></span></div>
                </div>

                <br>
                <div class="form-group text-center">
                   @if(CurrentUser())
                        <button data-toggle="modal" data-target="#speaker-ratings" class="btn btn-default">Rate Presenter</button>
                        @include('frontend.speakers.includes.speaker-ratings')
                    @endif
                    <a href="{{route('about.speakers')}}" class="btn btn-default">Back To Speakers</a>
                </div>
            </div>

            <div class="col-md-9">
                <div class="headline">
                    <h2> {{ $speaker->full_name }}</h2>
                    <br>
                    <br>
                    <small>{{ $speaker->job_title .', '.$speaker->organisation }}</small>
                </div>

                <p>{!! $speaker->bio !!}</p>

                <hr>
                <ul class="list-inline">
                    @if($speaker->website)
                        <li><a target="_blank" class="linked" target="_blank" href="{{ $speaker->website }}"><i class="fa-2x fa fa-globe color-green"></i></a></li>
                    @endif
                    @if($speaker->email)
                        <li><a target="_blank" href="mailto:{{$speaker->email}}"><i class="fa-2x fa fa-envelope color-green"></i></a></li>
                    @endif
                    @if($speaker->contact_number)
                        <li><a target="_blank" href="{{$speaker->contact_number}}"><i class="fa-2x fa fa-phone color-green"></i></a></li>
                    @endif
                    @if($speaker->facebook)
                        <li><a target="_blank" href="{{ $speaker->facebook }}"><i class="fa-2x fa fa-facebook color-green"></i></a></li>
                    @endif
                    @if($speaker->linkedin)
                        <li><a target="_blank" href="{{ $speaker->linkedin }}"><i class="fa-2x fa fa-linkedin color-green"></i></a> </li>
                    @endif
                    @if($speaker->twitter)
                        <li><a target="_blank" href="https://twitter.com/{{ $speaker->twitter }}"><i class="fa-2x fa fa-twitter color-green"></i></a> </li>
                    @endif
                    @if($speaker->yahoo)
                        <li><a target="_blank" href="{{ $speaker->yahoo }}"><i class="fa-2x fa fa-yahoo color-green"></i></a> </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    @include('global-includes.stars')
@endsection