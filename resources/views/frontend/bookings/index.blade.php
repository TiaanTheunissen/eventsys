@extends('layouts.frontend')

@section('title')
    Available Tickets
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('tickets') !!}
@endsection

@section('content')
    <div class="container content height-500">
        <div class="row">

            @unless(count($plans))
                @include('frontend.errors.error', ['text' => 'No Tickets found'])
            @endunless

            <p class="text-center"><i class="fa fa-info-circle"></i> Delegates who book and pay by <strong>30 September 2018</strong> will receive a free e-copy of this year's <br> Management of an Accounting Practice (MAP) Survey"</p>

            <div class="pricing-mega-v1">
                <div class="row">
                    @foreach($plans as $plan)
                        <div class="col-md-6" style="margin-bottom: 20px">
                            <div class="pricing">
                                <div class="pricing-head">
                                    <h3>
                                        {{ $plan->title }}
                                        {{--<span>Early bird prices expire on {{ date_format(\Carbon\Carbon::parse($plan->early_bird_expiry), 'd F Y')  }}</span>--}}
                                    </h3>
                                </div>
                                <table class="table table-striped" style="margin-bottom: 0px">
                                    <thead style="color: #888">
                                    <th>Available Options</th>
                                    @if($plan->early_bird_expiry >= \Carbon\Carbon::today())
                                        <th>Early Bird</th>
                                    @endif
                                    <th>Full Price</th>
                                    </thead>
                                    @if(count($plan->PlanOption))
                                        @foreach($plan->PlanOption as $option)
                                            <tr>
                                                <td class="padding-top-bottom-15"><i style="color: #72c02c" class="fa fa-suitcase"></i> {{ $option->title }}</td>
                                                @if($plan->early_bird_expiry >= \Carbon\Carbon::today())
                                                    <td class="padding-top-bottom-15">{{ $option->early_bird }}</td>
                                                @endif
                                                <td class="padding-top-bottom-15">{{ $option->full_price }}</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="padding-top-bottom-15" colspan="3">No options availble</td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                            <a href="{{ $plan->link }}" target="_blank" class="bnt btn-u btn-block"><i class="fa fa-shopping-cart"></i> Book Now</a>
                        </div>
                    @endforeach
                </div>
                <hr>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="headline"><h2>End of Year Functions</h2></div>
                <p>For any delegates who would like to host their year-end functions immediately after the close of the Bootcamp on 15 November, we have arranged availability with the restaurants in Menlyn Maine for you and your team.</p>
                <p>The Bootcamp will finish at approximately 13:00. To book your year-end function, simply purchase the R1000 voucher along with your ticket for the Bootcamp, choose your restaurant from the list below and phone the restaurant directly to confirm your booking.</p>
                <p>The voucher can then be used as payment or part-payment for your function.  Please note that vouchers must be booked and paid for no later than <strong>Friday 2 November</strong>.  </p>
                <table class="table">
                    <thead>
                    <th>Restaurant</th>
                    <th>Website</th>
                    <th>Telephone</th>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Turn N Tender</td>
                        <td><a href="https://turnntender.co.za/">https://turnntender.co.za/</a></td>
                        <td>012 348 0331</td>
                    </tr>
                    <tr>
                        <td>KOI</td>
                        <td><a href="https://koirest.co.za/">https://koirest.co.za/</a></td>
                        <td>012 348 4551</td>
                    </tr>
                    <tr>
                        <td>Old Town Italy</td>
                        <td><a href="https://oldtown.co.za/">https://oldtown.co.za/</a></td>
                        <td>012 348 2802</td>
                    </tr>
                    <tr>
                        <td>Ribs & Burgers</td>
                        <td><a href="https://ribsandburgers.com/za ">https://ribsandburgers.com/za </a></td>
                        <td>012 348 0029</td>
                    </tr>
                    <tr>
                        <td>Tashas</td>
                        <td><a href="http://www.tashascafe.com/ ">http://www.tashascafe.com/ </a></td>
                        <td>012 035 0367 (no bookings possible)</td>
                    </tr>
                    <tr>
                        <td>Mythos</td>
                        <td><a href="https://mythos.co.za ">https://mythos.co.za </a></td>
                        <td>012 348 3006</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection