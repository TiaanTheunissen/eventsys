@extends('layouts.frontend')

@section('title')
    Sponsors
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('sponsors.show', $sponsor->title) !!}
@endsection

@section('content')

    <div class="container content height-500">
        <div class="divider40"></div>
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-sm-3 col-xs-3">
                        <img src="{{$sponsor->SponsorImage()}}" class="thumbnail" style="max-width: 100%; max-height: 100%" alt="">
                        <p><a href="{{route('about.sponsors')}}" class="btn btn-u btn-block">Back to Sponsors</a></p>
                    </div>

                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
                        <div class="headline"><h2>{{ $sponsor->title }}</h2></div>
                        <p>{!! $sponsor->description !!}</p>

                        <hr>
                        <ul class="list-inline">
                            <li><i class="fa fa-globe color-green"></i> <a class="linked" target="_blank" href="{{ ($sponsor->website) ? : "#" }}">{{ ($sponsor->website) ? : "None" }}</a></li>
                            <li><i class="fa fa-phone color-green"></i> {{ ($sponsor->contact_number) ? : "None" }}</li>
                            <li><i class="fa fa-envelope color-green"></i> {{ ($sponsor->email) ? : "None" }}</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection