@extends('layouts.frontend')

@section('title')
    Sponsors
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('sponsors') !!}
@endsection

@section('content')
    <div class="container content height-500">
        <div class="row">
            <div class="col-md-12">

                @if($sponsorpage)
                    <h6 class="custom-font">{{ $sponsorpage->title }}</h6>
                    {!! $sponsorpage->content !!}
                @endif

                @unless(count($categories))
                    <br>
                    <br>
                    @include('frontend.errors.error', ['text' => 'No Sponsors found'])
                @endif

                <div class="row">
                    <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
                        @foreach($categories as $category)
                            <div class="headline"><h2>{{$category->title}}</h2></div>
                            @foreach($category->sponsor as $sponsor)
                                <div class="row clients-page">
                                    <div class="col-md-2">
                                        <img src="{{ $sponsor->SponsorImage() }}" class="img-responsive hover-effect" alt="">
                                    </div>
                                    <div class="col-md-10">
                                        <a href="{{ route('about.sponsors.show', $sponsor->slug) }}" style="text-decoration: none"><h3>{{ $sponsor->title }}</h3></a>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-globe color-green"></i> <a class="linked" target="_blank" href="{{ ($sponsor->website) ? : "#" }}">{{ ($sponsor->website) ? : "None" }}</a></li>
                                            <li><i class="fa fa-phone color-green"></i> {{ ($sponsor->contact_number) ? : "None" }}</li>
                                            <li><i class="fa fa-envelope color-green"></i> {{ ($sponsor->email) ? : "None" }}</li>
                                        </ul>
                                        {{ $sponsor->short_description }}
                                        <br>
                                        <br>
                                        <a href="{{ route('about.sponsors.show', $sponsor->slug) }}" class="btn btn-default">Read More</a>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection