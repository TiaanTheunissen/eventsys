@extends('layouts.frontend')

@section('title')
    Contact Us
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('contact') !!}
@endsection

@section('content')
    <!-- Google Map -->
    <div id="map" class="map">

    </div>
    <!-- End Google Map -->

    <div class="container content">
        <div class="row margin-bottom-30">
            <div class="col-md-9 mb-margin-bottom-30">
                <div class="headline"><h2>Contact Form</h2></div>
                <p> Drop us a line or just say Hello!</p>
                <br />

                {!! Form::open(array('route' => 'contact.store', 'class' => 'form')) !!}
                    <div class="form-group">
                        {!! form::label('name', 'Your name') !!}
                        {!! Form::input('text', 'name', null, ['class' => 'form-control', 'placeholder' => 'Enter your name']) !!}
                    </div>

                    <div class="form-group">
                        {!! form::label('email', 'Your Email Address') !!}
                        {!! Form::input('text', 'email', null, ['class' => 'form-control', 'placeholder' => 'Enter your email']) !!}
                    </div>

                    <div class="form-group">
                        {!! form::label('contact_message', 'Your Message') !!}
                        {!! Form::textarea('contact_message', null, ['class' => 'form-control', 'placeholder' => 'Enter your message']) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Send Email', ['class' => 'btn btn-default']) !!}
                    </div>
                {!! Form::close() !!}
            </div>

            <div class="col-md-3">
                <!-- Contacts -->
                <div class="headline"><h2>Contacts</h2></div>
                <ul class="list-unstyled who margin-bottom-30">
                    @if($eventInformation)
                        <li><i class="fa fa-user"></i> {{ $eventInformation->contact_name }}</li>
                        <li><a href="#"><i class="fa fa-home"></i>{{ $eventInformation->street_address }}
                                {{ $eventInformation->venue_city }}, {{ $eventInformation->venue_country }}</a>
                        </li>
                        <li><a href="#"><i class="fa fa-envelope"></i>{{ $eventInformation->email_address }}</a></li>
                        <li><a href="#"><i class="fa fa-phone"></i>{{ $eventInformation->contact_number }}</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        function initMap() {
            @if($eventInformation)
                var myLatLng = {lat: {{ ($eventInformation->latitude)? : -26.128266 }}, lng: {{ ($eventInformation->longitude)? : 28.09657 }}};
            @else
                var myLatLng = {lat: -26.128266, lng: 28.09657 };
            @endif

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: myLatLng
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                @if($eventInformation)
                    title: '{!! ($eventInformation->title)? : "Johannesburg and Cape Town"  !!}'
                @else
                    title: 'No title yet'
                @endif
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKSVJyeG80asrfY3E_NVrMx57YOgt22Wo&callback=initMap"
            type="text/javascript"></script>
@endsection