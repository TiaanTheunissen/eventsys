<div class="tag-box tag-box-v6">
    <h2>{{ $text }}, Please try again later</h2>
    <p>If you believe this is shown as error, Please contact the website administrator on
        <a href="mailto: support@website.com">support@website.com</a>
    </p>
</div>