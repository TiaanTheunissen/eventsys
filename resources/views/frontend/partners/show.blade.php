@extends('layouts.frontend')

@section('title')
    Event Partner {{ Carbon\Carbon::today()->year }}
@endsection
@section('breadcrumbs')
    {!! Breadcrumbs::render('partners.show', $partner->title) !!}
@endsection

@section('content')

    <div class="container content height-500">
        <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                        <img src="{{$partner->PartnerImage()}}" width="100%" class="thumbnail" alt="">
                        <p><a href="{{route('about.partners')}}" class="btn btn-u btn-block">Back to Partners</a></p>
                    </div>
                    <div class="col-md-9 col-sm-9 col-lg-9 col-xs-9">
                        <div class="headline"><h2>{{ $partner->title }}</h2></div>
                        {!! $partner->description !!}
                        <hr>
                        <ul class="list-inline">
                            <li><i class="fa fa-globe color-green"></i> <a class="linked" target="_blank" href="{{ ($partner->website) ? : "#" }}">{{ ($partner->website) ? : "None" }}</a></li>
                            <li><i class="fa fa-phone color-green"></i> {{ ($partner->contact_number) ? : "None" }}</li>
                            <li><i class="fa fa-envelope color-green"></i> {{ ($partner->email) ? : "None" }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection