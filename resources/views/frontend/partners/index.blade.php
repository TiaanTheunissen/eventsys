@extends('layouts.frontend')

@section('title')
    Partners
@endsection
@section('breadcrumbs')
    {!! Breadcrumbs::render('partners') !!}
@endsection

@section('content')
    <div class="container content height-500">
        <div class="row">
            <div class="col-md-12">

                @unless(count($partners))
                    @include('frontend.errors.error', ['text' => 'No Partners found'])
                @endunless

                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12">
                            @foreach($partners as $partner)
                                <div class="row clients-page">
                                    <div class="col-md-2">
                                        <img src="{{ $partner->PartnerImage() }}" class="img-responsive hover-effect" alt="">
                                    </div>
                                    <div class="col-md-10">
                                        <a href="{{ route('about.partners.show', $partner->slug) }}" style="text-decoration: none"><h3>{{ $partner->title }}</h3></a>
                                        <ul class="list-inline">
                                            <li><i class="fa fa-globe color-green"></i> <a class="linked" target="_blank" href="{{ ($partner->website) ? : "#" }}">{{ ($partner->website) ? : "None" }}</a></li>
                                            <li><i class="fa fa-phone color-green"></i> {{ ($partner->contact_number) ? : "None" }}</li>
                                            <li><i class="fa fa-envelope color-green"></i> {{ ($partner->email) ? : "None" }}</li>
                                        </ul>
                                        {{ $partner->short_description }}
                                        <br>
                                        <br>
                                        <a href="{{ route('about.partners.show', $partner->slug) }}" class="btn btn-default">Read More</a>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
            </div>
        </div>
    </div>
@endsection