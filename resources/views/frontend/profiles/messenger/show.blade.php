@extends('layouts.frontend')

@section('title')
    Subject: {{ str_limit($thread->subject, '20') }}
@endsection

@section('SubPage')
    Message: {{ str_limit($thread->subject, '20') }}
@endsection

@section('content')
    <!--=== Content ===-->
    <div class="container content height-500">
        <div class="col-md-12">
            @foreach($thread->messages as $message)
                <div class="row">
                    <div class="col-sm-1">
                        <div class="thumbnail">
                            <img src="{{ $message->user->UserImage() }}">
                        </div>
                    </div>

                    <div class="col-sm-11">
                        <div class="panel panel-custom panel-default">
                            <div class="panel-heading">
                                <strong>{!! $message->user->name !!}</strong>
                                <span class="text-muted pull-right"> Message: {!! $message->created_at->diffForHumans() !!}</span>
                            </div>
                            <div class="panel-body">
                                {!! $message->body !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <hr>
            <h6>Reply to Conversation</h6>
            {!! Form::open(['route' => ['profile.messages.update', $thread->id], 'method' => 'PUT']) !!}
        <!-- Message Form Input -->
            <div class="form-group">
                {!! Form::textarea('message', null, ['class' => 'form-control']) !!}
            </div>

            <!-- Submit Form Input -->
            <div class="form-group">
                {!! Form::submit('Submit', ['class' => 'btn-u form-control']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop