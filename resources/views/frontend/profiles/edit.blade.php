@extends('layouts.frontend')

@section('title')
    Edit Profile
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('edit', $user) !!}
@endsection

@section('content')
    <div class="container content height-500">
        <div class="row">
            @include('frontend.profiles.includes.sidebar')
            <div class="col-md-9">
                <div class="row">
                        {!! Form::model($user->profile, ['Method' => 'Post', 'route' => ['profile.update', $user->slug], 'files' => true, 'class' => 'sky-form', 'style' => 'border:none']) !!}
                        <div class="col-md-12">
                            <div class="headline"><h3>Personal Information</h3></div>
                        </div>

                        <div class="col-md-6">
                            <section>
                                <span class="label-custom">Full Name</span>
                                <label class="input">
                                    <i class="icon-prepend fa fa-user"></i>
                                    <input type="text" name="name" value="{{ $user->name }}">
                                </label>
                            </section>

                            <section>
                                <span class="label-custom">Your Location</span>
                                <label class="input">
                                    <i class="icon-prepend fa fa-globe"></i>
                                    <input type="text" name="location" value="{{ $user->profile->location }}">
                                </label>
                            </section>
                        </div>
                        <div class="col-md-6">
                            <section>
                                <span class="label-custom">Email Address</span>
                                <label class="input">
                                    <i class="icon-prepend fa fa-envelope"></i>
                                    <input type="email" name="email" value="{{ $user->email }}">
                                </label>
                            </section>

                            <section>
                                <span class="label-custom">Profile Picture</span>
                                <label for="file" class="input input-file">
                                    <div class="button"><input name="thumbnail" type="file" id="file" onchange="this.parentNode.nextSibling.value = this.value">Browse</div><input name="thumbnail" type="text" readonly="">
                                </label>
                            </section>
                        </div>

                        <div class="col-md-12">
                            <section>
                                <span class="label-custom">My Biography</span>
                                <div class="form-group">
                                {!! Form::textarea('bio', $user->bio, ['class' => 'description form-control']) !!}
                                </div>
                            </section>
                        </div>

                        <div class="col-md-12">
                            <section>
                                <div class="headline"><h3>Social Information</h3></div>
                            </section>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! form::label('twitter_username', 'Twitter Username') !!}
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-twitter"></i></span>
                                            {!! Form::input('text', 'twitter_username', null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {!! form::label('facebook_username', 'Facebook Username') !!}
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-facebook"></i></span>
                                            {!! Form::input('text', 'facebook_username', null, ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                {!! form::label('github_username', 'Github Username') !!}
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-github"></i></span>
                                    {!! Form::input('text', 'github_username', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="pull-right">
                                <button class="bnt btn-u">Update Profile</button>
                                <a href="#" class="btn-u btn-u-blue">Change Account Settings</a>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('frontend.partials.summernote')
    <script>
        $( document ).ready( function( )
        {
            $( '.delete' ).bootstrap_confirm_delete(
                    {
                        debug:              false,
                        heading:            '<div style="margin-bottom: -26px; margin-top: 0px">Account Removal</div>',
                        message:            'Please note that once your account is deleted you will not be able to access your account anymore. Should you wish to proceed, click on delete ',
                        data_type:          'post',
                        callback:           function ( event )
                        {},
                        delete_callback:    function(event) {
                            // grab original clicked delete button
                            var button = event.data.originalObject;
                            // execute delete operation
                            button.closest('form').submit();
                        },
                        cancel_callback:    function() { console.log( 'cancel button clicked' ); }
                    }
            );
        } );
    </script>
@endsection


