@extends('layouts.frontend')

@section('title')
    Account Overview
@endsection

@section('breadcrumbs')
    @if(auth()->check() && CurrentUser()->id === $user->id)
        {!! Breadcrumbs::render('dashboard', CurrentUser()->name) !!}
    @else
        {!! Breadcrumbs::render('account', $user->name) !!}
    @endif
@endsection

@section('content')
    <div class="container content height-500">
        <div class="row">
            @include('frontend.profiles.includes.sidebar')
            <div class="col-md-9 col-sm-9 col-xs-9 col-lg-9">
                <div class="row">
                    @if(auth()->check())
                        @if($user->id === CurrentUser()->id)
                            @include('frontend.partials.status')
                        @endif
                    @endif
                </div>

                @if(count($user->statuses))
                    @foreach($user->statuses as $post)
                        <div class="panel">
                            <div class="panel-body">
                                @include('frontend.partials.delete_status')
                                <p>{!! $post->status !!}</p>
                                    @if(auth()->check())
                                        @if(CurrentUser()->isFriendWith($user) || CurrentUser()->id === $user->id)
                                            <smal>
                                                @if($post->liked() == true)
                                                    <a href="#" class="no-hover"><i class="fa fa-thumbs-o-up"></i> You like this</a> &nbsp;
                                                    <a class="no-hover" href="{{ route('profile.unlike_status', $post) }}"><i class="fa fa-thumbs-o-down"></i> Unlike</a>
                                                @else
                                                    <a href="{{ route('profile.like_status', $post->id) }}" class="no-hover">
                                                        <i class="fa fa-thumbs-o-up"></i> Like
                                                    </a>
                                                @endif
                                            </smal>
                                        @endif
                                    @else
                                        <small>
                                            <a href="/login">Do you like this?</a>
                                        </small>
                                    @endif
                                    <span class="pull-right">
                                        <a href="#" class="no-hover"><i class="fa fa-thumbs-o-up"></i>
                                            {{ $post->TotalLikes() }}  |
                                            {!! $post->created_at->diffForhumans() !!}
                                            <i class="fa fa-clock-o"></i>
                                        </a>
                                    </span>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="tag-box tag-box-v2 margin-bottom-10">
                        <p>No status updates yet, check back later</p>
                    </div>
                @endif

            </div>
        </div>
    </div>
@endsection

@section('scripts')
    {{-- Includes the modal that displays the remove friend function. --}}
    @include('frontend.includes.delete')
@endsection