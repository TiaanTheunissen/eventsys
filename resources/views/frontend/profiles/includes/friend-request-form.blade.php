<!-- Shows the friend request button on user profile if not friedns, Else Pending or Unfriend. -->
@if(CurrentUser())
    @if($user->isFriendWith(CurrentUser()))
        @if(CurrentUser()->id !== $user->id)
            <div class="row">
                <div class="col-md-12">
                    {!! Form::open(['Method' => 'Post', 'route' => ['unfriend_user', $user->id]]) !!}
                    @include('frontend.profiles.friends.includes.form', ['SubmitButtonText' => 'Unfriend', 'ButtonClass' => 'delete btn-block rounded btn btn-danger'])
                    <a href="#" data-target="#Message_user_modal"
                       data-toggle="modal" class="btn btn-default from-control btn-block rounded">Message</a>
                    {!! Form::close() !!}
                    @include('frontend.profiles.messenger.includes.send-message-modal', ['submit' => 'Send Message'])

                    <div class="pull-right">

                    </div>
                </div>
            </div>
        @endif

    @elseif($user->hasFriendRequestFrom(CurrentUser()))
        <button class="btn btn-default btn-block">Friend request sent <i class="fa fa-check" style="color: #72c02c"></i>
        </button>
    @else
        @if(CurrentUser()->id !== $user->id)
            {!! Form::open(['Method' => 'Post', 'route' => ['send_a_friend_request', $user->id]]) !!}
            @include('frontend.profiles.friends.includes.form', ['SubmitButtonText' => 'Send Friend Request', 'ButtonClass' => 'btn btn-default btn-block'])
            {!! Form::close() !!}
        @endif
    @endif
@endif