<div class="col-md-3 col-xs-3 col-sm-3 col-lg-3">
    <img src="{{ $user->UserImage() }}" width="100%" class="img-responsive margin-bottom-40" alt="">
    <div class="tag-box tag-box-v1 margin-bottom-40 text-center">
        <h4>{{ $user->name }}</h4>
        <small>Member Since: {{ \Carbon\Carbon::parse($user->created_at)->diffForHumans() }}</small>
    </div>

    @if(auth()->check())
        @unless(CurrentUser()->id !== $user->id)
            <ul id="sidebar-nav" class="list-group sidebar-nav-v1" style="text-transform: capitalize">
                <li class="list-group-item {{isActive('profile/'.CurrentUser()->slug)}} ">
                    <a href="{{ route('profile.profile', CurrentUser()->slug) }}">Overview</a>
                </li>

                <li class="list-group-item list-toggle {{isActive('profile/'.CurrentUser()->slug.'/messages', true)}}">
                    <a data-toggle="collapse" data-parent="#sidebar-nav" href="#collapse-icons">Messages</a>

                    <ul id="collapse-icons" class="collapse">
                        <li class="{{ isActive('profile/'.CurrentUser()->slug.'/messages') }}">
                            <span class="badge badge-u hidden-xs hidden-sm rounded-2x">{{ CurrentUser()->newThreadsCount() }} new</span>
                            <a href="{{ route('profile.messages', CurrentUser()->slug) }}">Inbox </a>
                        </li>
                        <li class="{{ isActive('profile/'.CurrentUser()->slug.'/messages/create') }}">
                            <a href="{{ route('profile.messages.create', CurrentUser()->slug) }}"> New message</a>
                        </li>
                    </ul>
                </li>
                <li class="list-group-item list-toggle {{ isActive('profile/'.CurrentUser()->slug.'/friends', true) }}">
                    <a data-toggle="collapse" data-parent="#sidebar-nav" href="#friends">Friends</a>

                    <ul id="friends" class="collapse">
                        <li class="{{ isActive('profile/'.CurrentUser()->slug.'/friends') }}">
                            <span class="badge badge-u hidden-xs hidden-sm rounded-2x">Friends {{ CurrentUser()->getFriendsCount() }}</span>
                            <a href="{{ route('profile.friends', CurrentUser()->slug) }}">Friends </a>
                        </li>
                        <li class="{{ isActive('profile/'.CurrentUser()->slug.'/friend/requests') }}">
                            <span class="badge badge-u hidden-xs hidden-sm rounded-2x">{{ count(CurrentUser()->getFriendRequests()) }} new</span>
                            <a href="{{ route('profile.friendsRequests', CurrentUser()->slug) }}"> Friend Requests</a>
                        </li>
                    </ul>
                </li>
                {{--<li class="{{ isActive('profile/'.CurrentUser()->slug.'/friends') }} list-group-item">--}}
                {{--<span class="badge badge-u hidden-xs hidden-sm">Friends {{ CurrentUser()->getFriendsCount() }}</span>--}}
                {{--<a href="{{ route('profile.friends', CurrentUser()->slug) }}">Friends</a>--}}
                {{--</li>--}}
                {{--<li class="list-group-item"><a href="#">Account Settings</a></li>--}}
                <li class="list-group-item {{ isActive('profile/'. CurrentUser()->slug.'/edit') }}"><a href="{{ route('profile.edit', CurrentUser()->slug) }}">Edit Profile</a></li>
                <li class="list-group-item"><a href="#">Logout</a></li>
            </ul>
            @else
                <div class="profile-userbuttons">
                    @include('frontend.profiles.includes.friend-request-form')
                </div>
            @endunless
        @endif
</div>

{{--<div class="col-md-3">--}}

{{--<div class="profile-sidebar">--}}
{{--<div class="profile-side-top">--}}
{{--<div class="profile-userpic">--}}
{{--<img src="{{ $user->UserImage() }}" class="img-responsive" alt="">--}}
{{--</div>--}}

{{--<div class="profile-usertitle">--}}
{{--<div class="profile-usertitle-name">--}}
{{--{{ ($user->name)? : CurrentUser()->name }}--}}
{{--</div>--}}
{{--<div class="profile-usertitle-job">--}}
{{--Member Since:--}}
{{--{{ Carbon\Carbon::parse($user->created_at)->diffForHumans() }} <br>--}}
{{--Total Friends: {{ $user->getFriendsCount() }}--}}
{{--</div>--}}

{{--</div>--}}

{{--<div class="profile-userbuttons">--}}
{{--@include('frontend.profiles.includes.friend-request-form')--}}
{{--</div>--}}
{{--</div>--}}

{{--@if(Auth::check())--}}
{{--@unless(CurrentUser()->id !== $user->id)--}}
{{--<div class="profile-usermenu">--}}
{{--<ul class="nav">--}}
{{--<li style="display: none!important;">{{ $count = CurrentUser()->newMessagesCount }}</li>--}}

{{--<li class="{{isActive('profile/'.CurrentUser()->slug)}}">--}}
{{--<a href="{{route('profile.profile', CurrentUser()->slug)}}"><i class="fa fa-home"></i> Dashboard</a>--}}
{{--</li>--}}

{{--<li class="{{isActive('profile/'.CurrentUser()->slug.'/messages')}}">--}}
{{--<a href="{{route('profile.messages', CurrentUser()->slug)}}"><i class="fa fa-envelope"></i> My Messages</a>--}}
{{--</li>--}}

{{--<li class="{{ isActive('profile/'.CurrentUser()->slug.'/friends') }}">--}}
{{--<a href="{{ route('profile.friends', CurrentUser()->slug) }}">--}}
{{--<i class="fa fa-users"></i> My Friends--}}
{{--</a>--}}
{{--</li>--}}

{{--<li class="{{ isActive('profile/'.CurrentUser()->slug.'/friend/requests') }}">--}}
{{--<a href="{{ route('profile.friendsRequests', CurrentUser()->slug) }}">--}}
{{--<i class="fa fa-user-plus"></i> My Friend Requests--}}
{{--</a>--}}
{{--</li>--}}

{{--<li class="{{ isActive('profile/'.CurrentUser()->slug.'/edit') }}">--}}
{{--<a href="{{route('profile.edit', CurrentUser()->slug)}}">--}}
{{--<i class="fa fa-pencil"></i> Update My Profile--}}
{{--</a>--}}
{{--</li>--}}

{{--<li>--}}
{{--<a href="#"><i class="fa fa-cogs"></i> Privacy Settings</a>--}}
{{--</li>--}}

{{--<li><a href="/logout"><i class="fa fa-sign-out"></i> Logout</a></li>--}}
{{--</ul>--}}
{{--</div>--}}
{{--@endunless--}}
{{--@endif--}}
{{--@include('frontend.profiles.messenger.includes.send-message-modal', ['submit' => 'Send Message'])--}}
{{--@include('frontend.profiles.messenger.includes.send-message-modal', ['submit' => 'Send Message'])--}}
{{--</div>--}}
{{--</div>--}}

