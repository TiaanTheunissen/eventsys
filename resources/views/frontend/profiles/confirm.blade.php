@extends('layouts.frontend')

@section('title')
    Email confirmation
@endsection

@section('content')
    <div class="bg-image-v2 bg-image-v2-dark parallaxBg3">
        <div class="container">
            <div class="headline-center headline-light">
                <div class="col-md-6 col-md-offset-3 confirm-page-content">
                    <h2>Welcome <br> {{ ($user['name']? : "Name") }}</h2>

                    <p>
                        Thank you for signing up and joining our community where you would be able to view the
                        conference program, rate our speakers and many more features
                    </p>

                    <p>
                        We have sent you a welcome email to {{ ($user['email']? : "youremail@example.com") }}, Please click on the link to activate your account
                        before you would be able to log in.
                    </p>

                    <p>
                        Kind Regards, <br>
                        Eventsys Comunity
                    </p>

                </div>
            </div>
        </div>
    </div>
@endsection


