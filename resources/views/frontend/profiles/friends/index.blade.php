@extends('layouts.frontend')

@section('title')
    My Friends
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('friends', $user) !!}
@endsection

@section('content')
    <div class="container content height-500">
        <div class="row">
        @include('frontend.profiles.includes.sidebar')
            <div class="col-md-9">
                @if(count($user->getFriends()))
                    <div class="team-v6">
                        @foreach($user->getFriends()->chunk(4) as $chunk)
                            <div class="row">
                                @foreach($chunk as $friend)
                                    <a href="{{ route('profile.profile', $friend->slug) }}">
                                        <div class="col-md-2 col-sm-2 col-lg-2 col-xs-2 md-margin-bottom-50">
                                            <img class="img-responsive rounded thumbnail" src="{{ $friend->UserImage() }}" alt="">
                                            <span>{{ $friend->name }}</span>
                                            <small>Joined: {{ $friend->created_at->diffForHumans() }}</small>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="tag-box tag-box-v6">
                        <h2>Sorry, You have no friends at this time.</h2>
                        <p>To start adding friends, please visit the <a href="{{ route('members.show') }}">members</a> page</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
    {{--<div class="container visible-lg">--}}
        {{--<div class="row">--}}
            {{--<div class="row profile">--}}
                {{--@include('frontend.profiles.includes.sidebar')--}}
                {{--<div class="col-md-9">--}}
                    {{--<div class="profile-content">--}}
                        {{--<h6 class="highlight custom-font">My Friends</h6>--}}
                        {{--<hr>--}}
                        {{--<div class="friends-container">--}}
                            {{--<div class="row">--}}
                                {{--<!-- Check for current logged in users friends -->--}}
                                {{--@if(count($user->getFriends(10)))--}}
                                    {{--@foreach($user->getFriends()->chunk(2) as $chunk)--}}
                                        {{--@foreach($chunk as $friend)--}}
                                            {{--<a href="{{route('profile.profile', $friend->slug)}}">--}}
                                                {{--<div class="col-md-5 friend-container">--}}
                                                    {{--<img src="{{ $friend->UserImage() }}" style="border-radius: 50%" alt="{{ ucwords($friend->name) }}" width="23%"/>--}}
                                                    {{--<div class="friend-info">--}}
                                                        {{--{{ ucwords($friend->name) }} <br>--}}
                                                        {{--<span class="label label-default">Member Since {{ Carbon\Carbon::parse($friend->created_at)->diffForHumans() }}</span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</a>--}}
                                        {{--@endforeach--}}
                                    {{--@endforeach--}}

                                {{--@else--}}
                                    {{--<div class="col-md-12">--}}
                                        {{--<p>You currently have no friends available.</p>--}}
                                    {{--</div>--}}
                                {{--@endif--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
@endsection