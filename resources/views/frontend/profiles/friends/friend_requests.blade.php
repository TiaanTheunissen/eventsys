@extends('layouts.frontend')

@section('title')
    Friend Requests
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('friend_requests', CurrentUser()) !!}
@endsection

@section('content')
    <div class="container content height-500">
        <div class="row">
            @include('frontend.profiles.includes.sidebar')
            <div class="col-md-9">
                @if(count(CurrentUser()->getFriendRequests()))
                    @foreach(CurrentUser()->getFriendRequests()->chunk(4) as $chunk)
                        <div class="row team-v6 margin-bottom-60">
                            @foreach($chunk as $request)
                                <a class="no-hover" href="{{ route('profile.profile', \App\User::find($request->sender_id)->slug) }}">
                                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 md-margin-bottom-20">
                                        <img class="img-responsive rounded thumbnail" src="{{ \App\User::find($request->sender_id)->UserImage() }}" alt="">
                                        <span>{{ \App\User::find($request->sender_id)->name }}</span>
                                        <small>Joined: {{ \App\User::find($request->sender_id)->created_at->diffForHumans() }}</small>
                                        <ul class="list-inline social-icons-v1">
                                            @include('frontend.profiles.friends.includes.accept_friend_request')
                                        </ul>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    @endforeach
                @else
                    <div class="tag-box tag-box-v6">
                        <h2>Sorry, You have no friend requests at this time.</h2>
                        <p>To start adding friends, please visit the <a href="{{ route('members.show') }}">members</a> page</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
    {{--<div class="container visible-lg">--}}
        {{--<div class="row">--}}
            {{--<div class="row profile">--}}
                {{--@include('frontend.profiles.includes.sidebar')--}}
                {{--<div class="col-md-9">--}}
                    {{--<div class="profile-content">--}}
                        {{--<h6 class="highlight custom-font">New Friend Requests</h6>--}}
                        {{--<hr>--}}
                        {{--<!-- Check for any new friend requests and display the friend request accept form. -->--}}
                        {{--@if(CurrentUser())--}}
                            {{--@if(CurrentUser()->id == $user->id)--}}
                                {{--@if(count($user->getFriendRequests()))--}}
                                    {{--@foreach($user->getFriendRequests() as $friendRequest)--}}
                                        {{--<div class="friends-container">--}}
                                            {{--@include('frontend.profiles.friends.includes.accept_friend_request')--}}
                                        {{--</div>--}}
                                    {{--@endforeach--}}
                                    {{--@else--}}
                                    {{--<p>You have no new pending friend requests.</p>--}}
                                {{--@endif--}}
                            {{--@endif--}}
                        {{--@endif--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}
@endsection