<li>
    <a href="#">
        {!! Form::open(['Method' => 'Post', 'route' => ['accept_friend_request', $request->sender_id]]) !!}
        {!! Form::hidden('recipient_id', $request->id) !!}
        <button type="submit" style="background: none;border: none;"><i class="rounded-x fa fa-check"></i></button>
        {!! Form::close() !!}
    </a>
</li>
<li>
    <a href="#">
        {!! Form::open(['Method' => 'Post', 'route' => ['deny_friend_request', $request->sender_id]]) !!}
        {!! Form::hidden('recipient_id', $user->id) !!}
        <button type="submit" style="background: none;border: none;"><i class="rounded-x fa fa-close"></i></button>
        {!! Form::close() !!}
    </a>
</li>

{{--<div class="row">--}}
    {{--<div class=" col-md-12 friend-container">--}}
        {{--<img src="http://www.gravatar.com/avatar/{{md5($request->email)}}?d=http://s10.postimg.org/3s4sq7in9/user_icon.jpg" alt="{{$request->name}}"/>--}}
        {{--<div class="friend-info">--}}
            {{--{{ $user->thisUser($request->sender_id)->name }} <br>--}}
            {{--<span class="label label-default">Received: {{ Carbon\Carbon::parse($request->created_at)->diffForHumans() }}</span>--}}
        {{--</div>--}}
        {{--<div class="pull-right profile-accept-buttons">--}}
            {{--<div class="profile-accept-buttons-first">--}}
            {{--{!! Form::open(['Method' => 'Post', 'route' => ['accept_friend_request', $request->sender_id]]) !!}--}}
                {{--{!! Form::hidden('recipient_id', $user->id) !!}--}}
                {{--{!! Form::submit('Accept Friend Request', ['class' => 'btn btn-default', 'style' => 'background-color: #ef5505 !important; color:white; ']) !!}--}}
            {{--{!! Form::close() !!}--}}
            {{--</div>--}}

            {{--{!! Form::open(['Method' => 'Post', 'route' => ['deny_friend_request', $request->sender_id]]) !!}--}}
                {{--{!! Form::hidden('recipient_id', $user->id) !!}--}}
                {{--{!! Form::submit('Decline Friend Reques', ['class' => 'btn btn-default']) !!}--}}
            {{--{!! Form::close() !!}--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

