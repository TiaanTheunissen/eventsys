@extends('layouts.frontend')

@section('title')
    Email confirmation successful
@endsection

@section('content')
    <div class="bg-image-v2 bg-image-v2-dark parallaxBg3">
        <div class="container">
            <div class="headline-center headline-light">
                <div class="col-md-6 col-md-offset-3 confirm-page-content">
                    <h2>Success!</h2>
                    <p>
                        Thank you for confirming {{ ($user->email) ? : "email@example.com" }}
                    </p>

                    <p>
                       You can now login and enjoy all the features of our website
                    </p>

                    <p>
                        Kind Regards, <br>
                        Eventsys Comunity
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection


