@extends('layouts.frontend')

@section('title')
    <span style="text-transform: uppercase">Community Members</span>
@endsection

@section('breadcrumbs')
    {!! Breadcrumbs::render('members') !!}
@endsection

@section('content')

    <div class="container content height-500">

        @if(count($members))
            @foreach($members->chunk(4) as $chunk)
                <div class="row team-v4">
                    @foreach($chunk as $member)
                    <div class="col-md-3 col-sm-6 col-xs-6 ">
                        <a href="{{ route('profile.profile', $member->slug) }}" class="no-hover">
                            <img class="img-responsive img-bordered" src="{{$member->UserImage()}}" alt="">
                            <span>{{ $member->name }}</span>
                            <small>- Comunity Member -</small>
                            @if($member->verified == false)
                                <i class="label rounded"
                                    style="background-color: #e74c3c;
                                    padding-left: 5px;
                                    padding-right: 5px;
                                    padding-top: 4px;">
                                    Not Verified
                                </i>
                            @else
                                <i class="label rounded"
                                   style="background-color: #72c02c;
                                    padding-left: 5px;
                                    padding-right: 5px;
                                    padding-top: 4px;">
                                    Verified
                                </i>
                            @endif
                        </a>
                    </div>
                    @endforeach
                </div>
                <br>
                <br>
            @endforeach
        @else
            {{-- If no content is availble this partial will show--}}
            @include('frontend.errors.error', ['text' => 'No Members found'])
        @endif
    </div>
@endsection