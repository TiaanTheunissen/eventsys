@extends('layouts.frontend')

@section('content')
    @include('frontend.partials.slider')

    <div class="content-md">
        <div class="container">
            <div class="row text-center margin-bottom-60">
                @if(count($boxes))
                    @foreach($boxes as $box)
                        <a href="{{ $box->link }}" class="no-hover">
                            <div class="col-md-4 content-boxes-v6 md">
                                <i class="rounded-x fa {{$box->icon}}"></i>
                                <h1 class="title-v3-md text-uppercase margin-bottom-10">{{ $box->title }}</h1>
                                <p>{{ $box->short_description }}</p>
                            </div>
                        </a>
                    @endforeach
                @else
                    @for($i = 0; $i<3; $i++)
                        <a href="#">
                            <div class="col-md-4 content-boxes-v6 md">
                                <i class="rounded-x fa fa-code"></i>
                                <h1 class="title-v3-md text-uppercase margin-bottom-10"> Your Title</h1>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                    dolor. Aenean massa.
                                </p>
                            </div>
                        </a>
                    @endfor
                @endif
            </div>
        </div>

        <div class="bg-color-light">
            <div class="container content-sm">
                <div class="row">
                    <div class="col-md-6 md-margin-bottom-50">
                        @if($about)
                            <img alt="Image"
                                 src="{{ $about->thumbnail }}"
                                 class="img-responsive img-bordered full-width">
                        @else
                            <img src="/assets/frontend/smarty/assets/img/about_placeholder.jpg"
                                 class="img-responsive img-bordered full-width">
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="headline-left margin-bottom-30">
                            @if($about)
                                <h2 class="headline-brd" style="text-transform: capitalize;">{{ $about->title }}</h2>
                                <p>{!! str_limit($about->description, 700) !!}</p>
                            @else
                                <h2 class="headline-brd">Your Title</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium
                                    quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet
                                    nec,
                                </p>
                            @endif
                            <a class="btn-u btn-brd btn-brd-hover btn-u-dark" href="{{ route('about.show') }}">
                                Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="flat-testimonials bg-image-v1 parallaxBg margin-bottom-60">
            <div class="container">
                <div class="headline-center headline-light margin-bottom-60">
                    <h2>Some of our speakers</h2>
                    <p>Our speakers for this year's conference include international and local accounting practitioners, thought leaders, <br> advisory firms and technology providers</p>
                </div>

                <div class="row">
                    @if(count($featured_speakers))
                        @foreach($featured_speakers as $speaker)
                            <div class="col-sm-4">
                                <a href="{{ route('about.speakers.show', $speaker->slug) }}" class="no-hover">
                                    <div class="flat-testimonials-in md-margin-bottom-50">
                                        <img alt="" src="{{ $speaker->SpeakerImage() }}"
                                             class="rounded-x img-responsive img-bordered">
                                        <h3>{{$speaker->full_name}}</h3>
                                        <span class="color-white">{{ str_limit($speaker->job_title. ' ' .$speaker->organisation, 80) }}</span>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    @else
                        @for($i=0; $i<3; $i++)
                            <div class="col-sm-4">
                                <div class="flat-testimonials-in md-margin-bottom-50">
                                    <img alt="" src="/assets/frontend/smarty/assets/img/team/img1-sm.jpg"
                                         class="rounded-x img-responsive img-bordered">
                                    <h3>Speaker Title</h3>
                                    <span class="color-green">Speaker Job title, Company</span>
                                </div>
                            </div>
                        @endfor
                    @endif
                </div>
            </div>
        </div>

        <div class="container content">
            <div class="headline-center margin-bottom-60">
                <h2>Sponsors &capand; Partners</h2>
                {{--<p>--}}
                    {{--If you are going to use a <span class="color-green">passage of Lorem Ipsum</span>, you need to be--}}
                    {{--sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators--}}
                    {{--on the Internet tend to repeat predefined chunks as necessary, making <span class="color-green">this the first</span>--}}
                    {{--true generator on the Internet.--}}
                {{--</p>--}}
            </div>

            @if(count($images))
                @foreach($images->chunk(4) as $chunk)
                    <div class="row margin-bottom-30">
                        @foreach($chunk as $image)
                            <div class="col-sm-12 sm-margin-bottom-30 col-md-3 col-xs-12">
                                <span><img class="img-responsive img-bordered" src="{{ $image }}" alt=""></span>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            @else
                @for($i=0; $i<4; $i++)
                    <div class="col-sm-3 sm-margin-bottom-30">
                        <img class="img-responsive" src="/assets/frontend/smarty/assets/img/main/img15.jpg" alt="">
                    </div>
                @endfor
            @endif
        </div>
    </div>

    {{--<div style="background-position: 50% -121px;" class="parallax-counter-v3 parallaxBg">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-3 col-xs-6 md-margin-bottom-50">--}}
                    {{--<div class="features">--}}
                        {{--<span class="counter">2</span>--}}
                        {{--<span class="features-info">days</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-xs-6 md-margin-bottom-50">--}}
                    {{--<div class="features">--}}
                        {{--<span class="counter">30</span>--}}
                        {{--<span class="features-info">topics</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-xs-6">--}}
                    {{--<div class="features">--}}
                        {{--<span class="counter">40 +</span>--}}
                        {{--<span class="features-info">speakers</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-md-3 col-xs-6">--}}
                    {{--<div class="features">--}}
                        {{--<span class="counter">200 +</span>--}}
                        {{--<span class="features-info">delegates</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@stop