@extends('layouts.frontend')

@section('title')
    <span style="text-transform: uppercase">LOGIN TO YOUR ACCOUNT</span>
@endsection

@section('SubPage')
    Login
@endsection

@section('content')
    <div class="bg-image-v2 bg-image-v2-dark parallaxBg1">

        <div class="container">
            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                <div class="margin-bottom-30"></div>
                {!! Form::open(['Method' => 'Post', 'class' => 'sky-form']) !!}

                <fieldset style="background-color: rgba(255,255,255,.9)">
                    <div class="section">
                        <label class="input">
                            <i class="icon-prepend fa fa-user"></i>
                            <input type="email" name="email" placeholder="Email Address">
                        </label>
                    </div>
                    <div class="margin-bottom-20"></div>
                    <div class="section">
                        <label class="input">
                            <i class="icon-prepend fa fa-lock"></i>
                            <input type="password" name="password" placeholder="Password">
                        </label>
                    </div>
                    <div class="margin-bottom-20"></div>
                    <div class="section">
                        <label class="checkbox">
                            <input type="checkbox" name="remember" checked=""><i></i>Keep me logged in</label>
                    </div>
                    <div class="margin-bottom-20"></div>
                    <div class="section">
                        <button class="bnt btn-u" type="submit">Login</button>
                        <button class="btn-u btn-u-blue" type="submit">Forgot your password</button>
                    </div>
                    <div class="margin-bottom-20"></div>
                </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    {{--<div class="container"  style="background: url('/assets/frontend/images/uni.jpg') no-repeat; width: 100%; height: 50%; min-height: 650px">--}}
        {{--<div class="row">--}}
            {{--<div class="divider40" style="padding-top: 150px"></div>--}}
            {{--<div class="col-md-4 col-md-offset-4">--}}
                {{--<div class="login-panel panel panel-default" style="background-color: rgba(255, 255, 255, 0.92);">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h3 class="panel-title text-center">Welcome back, Please login</h3>--}}
                    {{--</div>--}}
                    {{--<div class="panel-body">--}}

                        {{--{!! Form::open(['Method' => 'Post']) !!}--}}
                            {{--<fieldset>--}}

                                {{--<div class="form-group">--}}
                                    {{--{!! form::label('email', 'Email Address') !!}--}}
                                    {{--{!! Form::input('text', 'email', null, ['class' => 'form-control']) !!}--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--{!! form::label('password', 'Password') !!}--}}
                                    {{--{!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}--}}
                                {{--</div>--}}

                                {{--<div class="checkbox">--}}
                                    {{--<label>--}}
                                        {{--<input name="remember" type="checkbox" value="Remember Me" checked>Remember Me--}}
                                    {{--</label>--}}
                                {{--</div>--}}

                                {{--<div class="form-group">--}}
                                    {{--{!! Form::submit('Login', ['class' => 'btn custom-btn-theme btn-block']) !!}--}}
                                {{--</div>--}}
                            {{--</fieldset>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection