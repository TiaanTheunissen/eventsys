@extends('layouts.frontend')

@section('title')
    Register
@endsection

@section('SubPage')
    Register
@endsection
@section('content')
    <div class="bg-image-v2 bg-image-v2-dark parallaxBg1" style="background-position: 50% -6px;">
        <div class="container">
            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                <div class="margin-bottom-30"></div>

                {!! Form::open(['Method' => 'Post', 'class' => 'sky-form']) !!}

                <fieldset style="background-color: rgba(255,255,255,.9)">
                    <div class="section">
                        <label class="input">
                            <i class="icon-prepend fa fa-user"></i>
                            <input type="text" name="name" placeholder="Full Name">
                        </label>
                    </div>
                    <div class="margin-bottom-20"></div>
                    <div class="section">
                        <label class="input">
                            <i class="icon-prepend fa fa-envelope"></i>
                            <input type="email" name="email" placeholder="Email Address">
                        </label>
                    </div>
                    <div class="margin-bottom-20"></div>

                    <div class="section">
                        <label class="input">
                            <i class="icon-prepend fa fa-lock"></i>
                            <input type="password" name="password" placeholder="Password">
                        </label>
                    </div>
                    <div class="margin-bottom-20"></div>
                    <div class="section">
                        <label class="input">
                            <i class="icon-prepend fa fa-lock"></i>
                            <input type="password" name="password_confirmation" placeholder="Password Confirmation">
                        </label>
                    </div>
                    <div class="margin-bottom-20"></div>

                    {{--<div class="section">--}}
                        {{--<label class="checkbox">--}}
                            {{--<input type="checkbox" name="terms" checked=""><i></i>I accept terms and conditions</label>--}}
                    {{--</div>--}}
                    {{--<div class="margin-bottom-20"></div>--}}

                    <div class="section">
                        <button class="bnt btn-u" type="submit" id="register" name="register">Register</button>
                        <span class="pull-right">
                            <a href="/login" class="btn-u btn-u-blue">Already registerd ?</a>
                        </span>
                    </div>
                    <div class="margin-bottom-20"></div>
                </fieldset>
                {!! Form::close() !!}
            </div><!--/Headline Center V2-->
        </div><!--/container-->
    </div>
    {{--<div class="container"  style="background: url('/assets/frontend/images/uni.jpg') no-repeat; width: 100%; height: 50%; min-height: 650px">--}}
        {{--<div class="row">--}}
            {{--<div class="divider40" style="padding-top: 150px"></div>--}}
            {{--<div class="col-md-4 col-md-offset-4">--}}
                {{--<div class="login-panel panel panel-default" style="background-color: rgba(255, 255, 255, 0.92);">--}}
                    {{--<div class="panel-heading">--}}
                        {{--<h3 class="panel-title text-center">Please fill out the following information</h3>--}}
                    {{--</div>--}}
                    {{--<div class="panel-body">--}}

                        {{--{!! Form::open(['Method' => 'Post']) !!}--}}
                        {{--<fieldset>--}}
                            {{--<div class="form-group">--}}
                                {{--{!! form::label('name', 'Full Name') !!}--}}
                                {{--{!! Form::input('text', 'name', null, ['class' => 'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--{!! form::label('email', 'Email Address') !!}--}}
                                {{--{!! Form::input('email', 'email', null, ['class' => 'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--{!! form::label('password', 'Password') !!}--}}
                                {{--{!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}--}}
                            {{--</div>--}}
                            {{--<div class="form-group">--}}
                                {{--{!! form::label('password_confirmation', 'Confirm Password') !!}--}}
                                {{--{!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}--}}
                            {{--</div>--}}

                            {{--<div class="form-group">--}}
                                {{--{!! Form::submit('Register', ['class' => 'btn custom-btn-theme btn-block']) !!}--}}
                            {{--</div>--}}
                        {{--</fieldset>--}}
                        {{--{!! Form::close() !!}--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

@endsection