<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <title>404 Error Page 6 | Unify - Responsive Website Template</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Web Fonts -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,700&amp;subset=cyrillic,latin">

    <!-- CSS Global Compulsory -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/style.css">

    <!-- CSS Header and Footer -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/headers/header-default.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/footers/footer-v1.css">

    <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/animate.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/line-icons/line-icons.css">
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/plugins/font-awesome/css/font-awesome.min.css">

    <!-- CSS Page Style -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/pages/page_404_error6.css">

    <!-- CSS Customization -->
    <link rel="stylesheet" href="/assets/frontend/smarty/assets/css/custom.css">
</head>

<body>
<div class="wrapper">
    <!--=== Header ===-->
    <div class="error-bg">
        <div class="container">
            <!--Error Block-->
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="error-v7">
                        <h1>That's an error!</h1>
                        <span class="sorry">The requested URL was not found on this server. That's all we know.</span>
                        <strong class="h1">404</strong> <br>
                        <a href="{{ route('home') }}" class="btn-u btn-u-lg rounded-4x btn-u">Back Home</a>
                    </div>
                </div>
            </div>
        </div><!--/container-->
    </div><!--/error-bg-->
    <!--End Error Block-->

    <!--=== Sticky Footer ===-->
    <div class="container sticky-footer">
        <ul class="list-unstyled social-links margin-bottom-30">
            <li><a href="#" class="fb"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" class="tw"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="gp"><i class="fa fa-google-plus"></i></a></li>
        </ul>
        <p class="copyright-space">
            2016 &copy; All Rights Reserved. <a href="#">Eventsys</a>
        </p>
    </div>
    <!--=== End Sticky Footer ===-->
</div><!--/wrapper-->

<!-- JS Global Compulsory -->
<script src="/assets/frontend/smarty/assets/plugins/jquery/jquery.min.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/jquery/jquery-migrate.min.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/bootstrap/js/bootstrap.min.js"></script>

<!-- JS Implementing Plugins -->
<script src="/assets/frontend/smarty/assets/plugins/back-to-top.js"></script>

<!-- JS Page Level -->
<script src="/assets/frontend/smarty/assets/js/app.js"></script>

<script>
    jQuery(document).ready(function() {
        App.init();
    });
</script>
<!--[if lt IE 9]>
<script src="/assets/frontend/smarty/assets/plugins/respond.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/html5shiv.js"></script>
<script src="/assets/frontend/smarty/assets/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->
</body>
</html>
