@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        @include('admin.partials.page-header', ['PageHeader' => 'Edit About Us'])

        <div class="col-md-10 col-xs-10 col-lg-10 col-sm-10">
            <div class="row">
                {!! Form::model($about, ['Method' => 'Post', 'route' => ['admin.pages.about.update', $about->id], 'files' => true]) !!}
                <div class="form-group">
                    {!! form::label('title', 'Title') !!}
                    {!! Form::input('text', 'title', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! form::label('description', 'Description') !!}
                    {!! Form::textarea('description', null, ['class' => 'description form-control']) !!}
                </div>

                <div class="form-group input-group image-preview">
                    <input type="text" name="thumbnail" value="{{ (isset($about->thumbnail)? $about->thumbnail : "") }}" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                    <span class="input-group-btn">

                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>

                        <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif"  name="thumbnail"/>
                    </div>
                    </span>
                </div>

                <div class="form-group">
                    {!! Form::submit('Save Changes', ['class' => 'btn btn-default']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('admin.partners.includes.summernote')
@endsection