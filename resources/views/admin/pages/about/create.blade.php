@extends('layouts.admin')

@section('content')
    <div id="page-wrapper">
        @include('admin.partials.page-header', ['PageHeader' => 'About us'])

        <div class="col-md-10 col-xs-10 col-lg-10 col-sm-10">
            <div class="row">
                {!! Form::open(['Method' => 'Post', 'route' => 'admin.pages.about.store', 'files' => true]) !!}
                <div class="form-group">
                    {!! form::label('title', 'Conference Title') !!}
                    {!! Form::input('text', 'title', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    {!! form::label('description', 'Description') !!}
                    {!! Form::textarea('description', null, ['class' => 'description form-control']) !!}
                </div>

                <label for="thumbnail">Thumbnail</label>
                    <div class="form-group input-group image-preview">
                        <input type="text" name="thumbnail" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                        <span class="input-group-btn">

                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>

                            <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif"  name="thumbnail"/>
                    </div>
                    </span>
                </div>

                <div class="form-group">
                    {!! Form::submit('Save Content', ['class' => 'btn btn-default']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @include('admin.partners.includes.summernote')
@endsection