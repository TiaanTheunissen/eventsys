<script>
    $(document).ready(function() {
        $('.description').summernote({
            height: 500,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true,                  // set focus to editable area after initializing summernote
            enterHtml: '<p><br></p>' // '<br>', '<p>&nbsp;</p>', '<p><br></p>', '<div><br></div>'
        });
    });
</script>