<?php

// Home
Breadcrumbs::register('home', function($breadcrumbs)
{
    $breadcrumbs->push('Home', route('home'));
});

// Home > About
Breadcrumbs::register('about', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About', route('about.show'));
});

// Home > About > Sponsors
Breadcrumbs::register('sponsors', function($breadcrumbs)
{
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Sponsors', route('about.sponsors'));
});

// Home > About > Sponsors > show
Breadcrumbs::register('sponsors.show', function($breadcrumbs, $sponsor)
{
    $breadcrumbs->parent('sponsors');
    $breadcrumbs->push($sponsor, route('about.sponsors.show', ['slug']));
});

// Home > About > Sponsors
Breadcrumbs::register('partners', function($breadcrumbs)
{
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Partners', route('about.partners'));
});

// Home > About > Sponsors > show
Breadcrumbs::register('partners.show', function($breadcrumbs, $partner)
{
    $breadcrumbs->parent('partners');
    $breadcrumbs->push($partner, route('about.partners.show', ['slug']));
});

// Home > About > speakers
Breadcrumbs::register('speakers', function($breadcrumbs)
{
    $breadcrumbs->parent('about');
    $breadcrumbs->push('Speakers', route('about.speakers'));
});

// Home > About > speakers > show
Breadcrumbs::register('speakers.show', function($breadcrumbs, $speaker)
{
    $breadcrumbs->parent('speakers');
    $breadcrumbs->push($speaker, route('about.speakers.show', ['slug']));
});

// Home > Agenda
Breadcrumbs::register('schedule', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Agenda', route('schedule'));
});

// Home > Members
Breadcrumbs::register('members', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Members', route('members.show'));
});

// Home > members > Account
Breadcrumbs::register('account', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('members');
    $breadcrumbs->push($user, route('members.show'));
});

// Home > Profile > My Account
Breadcrumbs::register('my_account', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('My Account', route('members.show'));
});

// My Account > dashboard
Breadcrumbs::register('dashboard', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('my_account');
    $breadcrumbs->push($user, route('members.show'));
});

// My Account > dashboard > friends
Breadcrumbs::register('friends', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('my_account');
    $breadcrumbs->push('My Friends', route('profile.friends', $user->slug));
});

// My Account > dashboard > friends > requests
Breadcrumbs::register('friend_requests', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('friends', $user);
    $breadcrumbs->push('My Friend Requests', route('profile.friendsRequests', $user->slug));
});

// My Account > Edit Profile
Breadcrumbs::register('edit', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('my_account', $user);
    $breadcrumbs->push('Edit Profile', route('profile.friendsRequests', $user->slug));
});

// Home > Tickets
Breadcrumbs::register('tickets', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Tickets', route('bookings.show'));
});

// Home > Contact Us
Breadcrumbs::register('contact', function($breadcrumbs)
{
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Contact us', route('contact'));
});