<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AboutUs;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Intervention\Image\Facades\Image;
use Laracasts\Flash\Flash;

class AdminAboutUsController extends Controller
{
    public function edit()
    {
        $about = AboutUs::first();
        return view('admin.pages.about.edit', compact('about'));
    }

    public function create()
    {
       return view('admin.pages.about.create');
    }

    public function store(Request $request)
    {
        AboutUs::truncate();

        $about = $request->except(['_token', 'thumbnail']);
        $about = $this->getFile($about);
        AboutUs::create($about);

        Flash::message('Your Changes has bee saved!');
        return redirect('admin/view');
    }

    public function update()
    {
        $about = AboutUs::find(1)->first();
        $input = Input::except('thumbnail', '_token');
        $this->updateFile($about);
        $about->update($input);

        Flash::message('Your Changes has bee saved!');
        return redirect()->back();
    }

    public function createFolder()
    {
        $path = 'assets/admin/images/about';
        if (!file_exists($path)) File::makeDirectory($path, 0775, true, true);
    }

    public function inputFile($folder)
    {
        $thumbnail = Input::file('thumbnail');
        $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
        $path = public_path($folder . $filename);
        Image::make($thumbnail->getRealPath())->resize('555', '304')->fill()->save($path);
        $thumbnail = ['thumbnail' => $folder . $filename];
        return $thumbnail;
    }

    public function getFile($about)
    {
        $this->createFolder();
        $folder = '/assets/admin/images/about/';
        if (Input::hasfile('thumbnail')) {
            $thumbnail = $this->inputFile($folder);
            $about = array_merge($about, $thumbnail);
            return $about;
        }
        return $about;
    }

    public function updateFile($about)
    {
        $folder = '/assets/admin/images/about/';
        if (Input::hasfile('thumbnail')) {
            File::delete(public_path() . $about->thumbnail);
            $thumbnail = $this->inputFile($folder);
            $about->update($thumbnail);
        };
    }
}
