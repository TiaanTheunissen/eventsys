<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Laracasts\Flash\Flash;

class VerifyUserController extends Controller
{
    public function verify($token)
    {
        $user = User::whereToken($token)->firstorFail()->confirmEmail();
        return view('frontend.profiles.confirmed_success', compact('user'));
    }

    public function confirm()
    {
        $user = null;
        return view('frontend.profiles.confirm', compact('user'));
    }

    public function confirmed_success()
    {
        $user = User::first();
        return view('frontend.profiles.confirmed_success', compact('user'));
    }
}
