<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\LikedStatus;
use App\Status;
use Illuminate\Http\Request;

use App\Http\Requests;

class StatusController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, ['status' => 'required']);
        $user = CurrentUser();
        $status = $this->clean($request);
        $user->statuses()->create($status);
        return back();
    }

    public function destroy($status)
    {
        $status = Status::findorFail($status);
        $status->delete();
        return back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function clean(Request $request)
    {
        $status = preg_replace('/[^A-Za-z0-9\. -]/', '', $request->all());
        return $status;
    }

    public function LikedThis($status)
    {
        $post = Status::find($status);
        $post->likes()->attach(CurrentUser());
        return back();
    }

    public function UnlikeThis($status)
    {
        $post = LikedStatus::where('user_id', CurrentUser()->id)
                ->where('status_id', $status)
                ->first();
        $post->delete();
        return back();
    }
}
