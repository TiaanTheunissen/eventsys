<?php
/**
 * Created by PhpStorm.
 * User: Tiaan Theunissen
 * Date: 2016/08/10
 * Time: 2:59 PM
 */

namespace App\Mailers;


class AppMailers
{
    protected $mailer;

    protected $from = 'support@website.com';

    protected $to;

    protected $view;

    protected $data = [];

    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function sendEmailConfirmationTo(User $user)
    {
        $this->to = $user->email;
        $this->view = 'emails.confirm';
        $this->data = compact('user');

        $this->deliver();
    }

    public function deliver()
    {
        $this->mailer->send($this->view, $this->data, function ($message){
            $message->from($this->from, 'Administrator')
                ->to($this->to);
        });
    }
}