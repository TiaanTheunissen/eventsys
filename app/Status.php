<?php

namespace App;

use App\Models\LikedStatus;
use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'status';
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * a status is owned by a  user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function likes()
    {
        return $this->belongsToMany(User::class);
    }

    public function liked()
    {
        if (LikedStatus::where('user_id', CurrentUser()->id)
            ->where('status_id', $this->id)
            ->first()){
            return true;
        }
    }

    public function TotalLikes()
    {
        return count($this->likes()->get());
    }
}


