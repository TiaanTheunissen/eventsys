<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Partner extends Model implements SluggableInterface
{
    use SluggableTrait, SoftDeletes;
    protected $table = 'partners';
    protected $dates = ['deleted_at'];
    protected $fillable = ['title', 'slug', 'email', 'website', 'thumbnail', 'description', 'short_description', 'contact_number'];
    protected $sluggable = ['build_from' => 'title', 'save_to' => 'slug',];

    public function scopePartnerImage()
    {
        return $this->thumbnail ?: '/assets/frontend/placeholder/partner.jpg';
    }
}