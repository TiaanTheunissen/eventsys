<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LikedStatus extends Model
{
    protected $table = 'status_user';
    protected $guarded = [];
}
