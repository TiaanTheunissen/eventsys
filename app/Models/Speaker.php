<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Ghanem\Rating\Contracts\Rating;
use Ghanem\Rating\Traits\Ratingable as RatingTrait;
class Speaker extends Model implements SluggableInterface
{
    use RatingTrait;
    use SluggableTrait;
    protected $table = 'speakers';
    protected $guarded = [];
    protected $sluggable = ['build_from' => 'full_name', 'save_to' => 'slug',];

    public function panel()
    {
        return $this->belongsToMany(PlenaryPanel::class)->withTimestamps();
    }

    public function streamPanel()
    {
       return $this->belongsToMany(StreamPanel::class)->withTimestamps();
    }

    public function scopeSpeakerImage()
    {
        return $this->thumbnail ? : '/assets/frontend/smarty/assets/img/team/img1-sm.jpg';
    }
}
