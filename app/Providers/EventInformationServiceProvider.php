<?php

namespace App\Providers;

use App\Http\Controllers\Frontend\ContactController;
use App\Models\AboutUs;
use App\Models\EventInfo;
use App\Models\Speaker;
use App\Models\SponsorPageContent;
use App\Models\Social;
use App\User;
use Illuminate\Support\ServiceProvider;

class EventInformationServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.*', function($view)
        {
            $view->with('eventInformation', EventInfo::first());
            $view->with('sponsorpage', SponsorPageContent::first());
            $view->with('about', AboutUs::first());
            $view->with('socials', Social::all());
            $view->with('new_members', User::all()->sortByDesc('created_at')->take(3));
            $view->with('featured_speakers', Speaker::where('featured', true)->orderBy('position')->get()->take(3));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
